section .text 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
    .counter:
	cmp byte [rdi + rax], 0
	je .end
	inc rax
	jmp .counter
    .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
    .end:
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	add rsp, 8
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, '\n'
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push rbx
	mov rax, rdi
	xor rdx, rdx
	mov rbx, rsp
	sub rsp, 24
	mov r8, 10
	mov byte [rbx], 0
	dec rbx
    .loop:
    	cmp rax, 10
	jb .end
	div r8
	add rdx, '0'
	mov byte [rbx], dl
	dec rbx
	xor rdx, rdx
	jmp .loop
    .end:
	add rax, '0'
	mov byte [rbx], al
	mov rdi, rbx
    	call print_string
	add rsp, 24
	pop rbx
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jnl print_uint
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	jmp print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
     .loop:
     	mov byte al, [rdi]
	cmp byte [rsi], al 
	jne .notEqual
	test al, al
	je .equal
	inc rdi
	inc rsi
	jmp .loop
     .equal:
	mov rax, 1
	ret
      .notEqual:
	xor rax, rax
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	xor rax, rax ; 0 - номер системного вызова read
	xor rdi, rdi ; 0 - номер stdin
   	mov rsi, rsp
	mov rdx, 1
	syscall
	cmp rax, -1
	jne .done
	xor rax, rax  ; 0 - достигнут конец потока
	add rsp, 8
	ret
    .done:
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	xor r14, r14
    .skipSpaces:
	call read_char
	cmp rax, ' '
	je .skipSpaces
	cmp rax, '	'
	je .skipSpaces
	cmp rax, '\n'
	je .skipSpaces
	cmp rax, 0
	je .fail
    .readNext:	
	mov byte [r12 + r14], al
	inc r14
	cmp r13, r14
	je .fail
	call read_char
	cmp rax, ' '
	je .done
	cmp rax, '	'
	je .done
	cmp rax, '\n'
	je .done
	cmp rax, 0
	je .done
	jmp .readNext
    .done:
	mov byte [r12 + r14], 0
	mov rax, r12
	mov rdx, r14
	pop r14
	pop r13
	pop r12
	ret
    .fail:
	xor rax, rax
	mov rdx, r14
	pop r14
	pop r13
	pop r12
	ret	

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rcx, rcx
	xor rdx, rdx
	xor rax, rax
    .loop:	
	mov al, [rdi + rcx]
	test al, al
	je .end
	cmp al, '0'
	jb .end
	cmp al, '9'
	ja .end
	inc rcx
	sub rax, '0'
	push rax
	imul rdx, 10
	pop rax
	add rdx, rax
	jmp .loop
    .end:
	mov rax, rdx
	mov rdx, rcx
	ret	

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
	mov al, [rdi]
	cmp al, '-'
	je .negative
	cmp al, '+'
	je .positive
	jmp parse_uint
    .positive:
	inc rdi
	jmp parse_uint
    .negative:
	inc rdi
	call parse_uint
	test rdx, rdx
	je .fail
	neg rax
	inc rdx	
    .fail:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor rcx, rcx
	test rdx, rdx
	ja .loop
	ret
    .loop:
	mov byte al, [rdi + rcx]
	mov byte [rsi + rcx], al
	inc rcx
	test al, al
	je .done
	cmp rdx, rcx
	je .fail
	jmp .loop
    .fail:
	xor rax, rax
	ret
    .done:
	mov rax, rcx
	ret
